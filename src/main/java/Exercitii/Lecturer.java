package Exercitii;

public class Lecturer extends Person {
    private String specialization;
    private int salary;


    public Lecturer(){
        super();
    }
    public Lecturer(String specialization, int salary, String name, String adress){
        super(name, adress);
        this.specialization= specialization;
        this.salary= salary;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Lecturer{" +
                "specialization='" + specialization + '\'' +
                ", salary=" + salary +
                super.toString() +
                '}';
    }
}
