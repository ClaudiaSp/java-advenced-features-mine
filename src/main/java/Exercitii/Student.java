package Exercitii;

public class Student extends Person {
    private String study;
    private int year;
    private int price;


    public Student(){
        super();

    }

    public Student(String study, int year, int price, String name, String adress){
        super (name, adress);
        this.study = study;
        this.year = year;
        this.price = price;
    }

    public String getStudy() {
        return study;
    }

    public void setStudy(String study) {
        this.study = study;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Student{" +
                "study='" + study + '\'' +
                ", year='" + year + '\'' +
                ", price='" + price + '\'' +
                super.toString() +
                '}';
    }
}
