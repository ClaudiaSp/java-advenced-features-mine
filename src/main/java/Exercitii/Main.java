package Exercitii;

public class Main {
    public static void main(String[] args) {
        Student man1 = new Student("law", 5, 2500, "Cornel", "Iasi");
        Lecturer man2 = new Lecturer("medic", 5000, "Teodora", "Iasi");
        showLecturer(man2);
        showStudent(man1);

    }

    public static void showStudent(Student student){
        System.out.println(student.toString());
    }
    public static void showLecturer(Lecturer lecturer){
        System.out.println(lecturer.toString());
    }

}
 

