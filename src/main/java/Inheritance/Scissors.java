package Inheritance;

public class Scissors extends Tool{
    private String color;
    public Scissors(){
        super();
        this.color = "";
    }



    public Scissors(String name, String scope, String price, String color) {
        super(name, scope, price);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String     toString() {
        return "Scissors{" +
                "color='" + color + '\'' +
                ", name='" + name + '\'' +
                ", scope='" + scope + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
