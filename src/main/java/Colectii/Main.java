package Colectii;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> visitedCountries = new ArrayList<String>();
        visitedCountries.add("Germania");
        visitedCountries.add("Franta");
        visitedCountries.add("Spania");
        visitedCountries.remove("Franta");

        for (String country : visitedCountries) {
            System.out.println(country + " ");
        }
        System.out.println("------------");
        System.out.println(visitedCountries.size());
        System.out.println("------------");


        List <String> countries = new ArrayList<String>();
        countries.add("Anglia");
        countries.add("Elvetia");
        countries.add("Finlanda");
        visitedCountries.addAll(countries);

        Iterator<String> iterator = visitedCountries.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("------------");

        Iterator<String> iterator2 = countries.iterator();
        while(iterator2.hasNext()){
            System.out.println(iterator2.next());
        }
        System.out.println("------------");

    }
}
